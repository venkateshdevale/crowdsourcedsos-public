package ui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.weloftlabs.crowdsourcedsos.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import managers.ProfileManager;
import model.User;
import utils.Constants;

import static ui.VictimDetailsActivity.BUNDLE_KEY_OBJECT_ID;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, ProfileManager.ProfileDownloaderCallback {

    private static final String BUNDLE_KEY_LAT = "lat_victim";
    private static final String BUNDLE_KEY_LON = "lon_victim";
    private static final String BUNDLE_KEY_NAME = "name_victim";

    private Toolbar mToolbar;
    private GoogleMap mMap;
    private double mLat;
    private double mLon;
    private String mVictimName;
    private String mUserId;
    private Timer mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            mUserId = extras.getString(Constants.SharedPrefKeys.USER_ID);

            ProfileManager.getVictimProfileDetails(mUserId, this);

            ProfileManager.getVictimAudioList(mUserId, this);

            ProfileManager.getVictimLocationList(mUserId, this);

            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {

                    ProfileManager.getVictimAudioList(mUserId, MapsActivity.this);

                    ProfileManager.getVictimLocationList(mUserId, MapsActivity.this);
                }
            }, 50000, 50000);


        } else {

            mLat = 35;
            mLon = 13;
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng victimLoc = new LatLng(mLat, mLon);
        mMap.addMarker(new MarkerOptions().position(victimLoc).title(mVictimName));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(victimLoc));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                Intent intent = new Intent(MapsActivity.this, VictimDetailsActivity.class);
                intent.putExtra(BUNDLE_KEY_OBJECT_ID, mUserId);
                startActivity(intent);
                return false;
            }
        });
    }

    @Override
    public void onDownloadProfile(User user) {
        mVictimName = user.name;

    }

    @Override
    public void onDownloadLocationList(List<ParseObject> locationsList) {
        if (locationsList.size() != 0) {
            ParseObject locationObj = locationsList.get(locationsList.size() - 1);
            ParseGeoPoint point = locationObj.getParseGeoPoint("victimLocationRecord");
            mLat = point.getLatitude();
            mLon = point.getLongitude();

            onMapReady(mMap);
            Toast.makeText(MapsActivity.this, "Victim location updated", Toast.LENGTH_SHORT).show();


        }
    }

    @Override
    public void onDownloadAudioList(List<ParseObject> audioList) {
        if (audioList.size() > 0) {
            ParseObject audioObj = audioList.get(0);

            Toast.makeText(this, "Received audio", Toast.LENGTH_LONG).show();

            ParseFile outputFile = audioObj.getParseFile("victim_record");
            try {
                play3GP(outputFile.getData());
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onDestroy() {
        if (mTimer != null)
            mTimer.cancel();
        super.onDestroy();
    }

    private void play3GP(byte[] soundByteArray) {
        try {
            File temp3GP = File.createTempFile("temp_audio", "3gp", getCacheDir());
            temp3GP.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(temp3GP);
            fos.write(soundByteArray);
            fos.close();

            MediaPlayer mediaPlayer = new MediaPlayer();

            FileInputStream fis = new FileInputStream(temp3GP);
            mediaPlayer.setDataSource(fis.getFD());

            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException ex) {
            String s = ex.toString();
            ex.printStackTrace();
        }
    }

}
