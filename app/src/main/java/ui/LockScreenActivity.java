package ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.weloftlabs.crowdsourcedsos.R;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import managers.SharedPreferenceManager;
import services.LocationUpdateService;
import services.PanicService;
import services.RecordingService;
import utils.Constants;
import utils.Logger;
import utils.unlock.UnlockBar;

import static android.widget.Toast.makeText;
import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

public class LockScreenActivity extends BaseActivity implements
        RecognitionListener {

    private static Context sContext;
    private TextView mTimerTv;
    private TextView mAlertTv;
    private TextView mHintTv;
    private Timer timer;
    private ImageView mAlertBtn;
    private TimerTask timerTask;
    private final Handler handler = new Handler();

    /* Named searches allow to quickly reconfigure the decoder */
    private static String KWS_SEARCH;
    /* Keyword we are looking for to activate menu */
    private static String KEYPHRASE;

    private SpeechRecognizer recognizer;
    private HashMap<String, Integer> captions;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        KWS_SEARCH = SharedPreferenceManager.newInstance(this).getSavedValue(Constants.SharedPrefKeys.PHRASE);
        KEYPHRASE = KWS_SEARCH;

        initialize();

        setContentView(R.layout.activity_lockscreen);

        mTimerTv = (TextView) findViewById(R.id.timer_tv);
        mAlertTv = (TextView) findViewById(R.id.alert_status_tv);
        mAlertBtn = (ImageView) findViewById(R.id.alert_btn);
        mHintTv = (TextView) findViewById(R.id.caption_text);
        mHintTv.setText("Preparing the recognizer");

        mAlertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recognizer.stop();
                triggerAlert();

            }
        });

        UnlockBar unlock = (UnlockBar) findViewById(R.id.unlock);
        unlock.setOnUnlockListener(new UnlockBar.OnUnlockListener() {
            @Override
            public void onUnlock() {

                finish();
            }
        });

        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(LockScreenActivity.this);
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                if (result != null) {
                    ((TextView) findViewById(R.id.caption_text))
                            .setText("Failed to init recognizer " + result);
                } else {
                    switchSearch(KWS_SEARCH);
                }
            }
        }.execute();
    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();
        Logger.d("Voice-->" + text);
        if (text.equals(KEYPHRASE)) {
            Logger.d("Voice-->" + text);
            recognizer.stop();

            triggerAlert();
        } else
            ((TextView) findViewById(R.id.result_text)).setText(text);

    }

    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        Logger.d("onResult");
        ((TextView) findViewById(R.id.result_text)).setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
            makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBeginningOfSpeech() {
        Logger.d("onBeginningOfSpeech");

    }

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        Logger.d("onEndOfSpeech");

    }

    private void switchSearch(String searchName) {
        Logger.d("switchSearch");
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);

        ((TextView) findViewById(R.id.caption_text)).setText("To trigger panic mode, shout \"" + KEYPHRASE + "\"");
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        Logger.d("setupRecognizer");
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        recognizer = defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                        // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                .setRawLogDir(assetsDir)
                        // Threshold to tune for keyphrase to balance between false alarms and misses
                .setKeywordThreshold(1e-45f)
                        // Use context-independent phonetic search, context-dependent is too slow for mobile
                .setBoolean("-allphone_ci", true)
                .getRecognizer();
        recognizer.addListener(this);

        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

    }

    @Override
    public void onError(Exception error) {
        Logger.d("onError");
        ((TextView) findViewById(R.id.caption_text)).setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        Logger.d("onTimeout");
        switchSearch(KWS_SEARCH);
    }

    private void initialize() {
        sContext = this;
        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        // Make this activity FullScreen.
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

    }

    private void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        Calendar calendar = Calendar.getInstance();
                        mTimerTv.setText(calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE));
                    }

                });

            }

        };

    }

    private void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 0, 60000); //
    }

    private void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stoptimertask();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recognizer.cancel();
        recognizer.shutdown();
        LocalBroadcastManager.getInstance(LockScreenActivity.this)
                .sendBroadcast(new Intent(PanicService.KILL_PANIC_SERVICES));
        sContext = null;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void triggerAlert() {
        // Make Alert status textview visible
        mAlertBtn.setEnabled(false);
        mAlertTv.setVisibility(View.VISIBLE);
        mAlertBtn.setBackground(getResources().getDrawable(R.drawable.alert_on));
        mHintTv.setText("Panic button triggered");

        startService(new Intent(this, LocationUpdateService.class));
        startService(new Intent(this, RecordingService.class));

    }

    public static Context getContext() {
        return sContext;
    }
}
