package ui;

import android.support.v7.app.AppCompatActivity;

import utils.NotificationUtils;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */
public class BaseActivity extends AppCompatActivity {


    @Override
    protected void onResume() {
        super.onResume();

        NotificationUtils.enableLocationIfDisabled(this);
    }
}
