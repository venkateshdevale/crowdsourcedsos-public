package ui;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.weloftlabs.crowdsourcedsos.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import managers.ProfileManager;
import managers.ProfileManager.ProfileDownloaderCallback;
import model.User;

/**
 * Created by sree on 4/10/15.
 */
public class VictimDetailsActivity extends BaseActivity implements ProfileDownloaderCallback {

    public static String BUNDLE_KEY_OBJECT_ID = "object_id";

    private Timer mTimer;
    private Toolbar mToolbar;
    private SimpleDraweeView profilePic;
    private TextView name;
    private TextView contact;
    private TextView ec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victim_details);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        profilePic = (SimpleDraweeView) findViewById(R.id.profile_pic);
        name = (TextView) findViewById(R.id.info_text);
        contact = (TextView) findViewById(R.id.contact);
        ec = (TextView) findViewById(R.id.ec_contact);

        final Bundle extras = getIntent().getExtras();

        if (extras != null) {
            ProfileManager.getVictimProfileDetails(extras.getString(BUNDLE_KEY_OBJECT_ID), this);

            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {

                    ProfileManager.getVictimAudioList(extras.getString(BUNDLE_KEY_OBJECT_ID), VictimDetailsActivity.this);

                }
            }, 50000, 50000);

        }
    }

    @Override
    public void onDownloadProfile(User user) {

        findViewById(R.id.progressBar).setVisibility(View.GONE);

        Uri uri = Uri.parse(user.url);
        profilePic.setImageURI(uri);

        name.setText(user.name);
        contact.setText("Contact No: " + user.contactNo);
        ec.setText("Emergency Contact: " + user.ecContact);

    }

    @Override
    public void onDownloadLocationList(List<ParseObject> locationsList) {

    }

    @Override
    public void onDownloadAudioList(List<ParseObject> audioList) {

    }

}
