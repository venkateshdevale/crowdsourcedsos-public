package ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.weloftlabs.crowdsourcedsos.R;

import managers.SharedPreferenceManager;
import services.CommonService;
import utils.Constants;


/**
 * Created by Venkatesh Devale on 03-10-2015.
 */
public class SettingsActivity extends BaseActivity {

    private Toolbar mToolbar;
    private EditText mPhraseEt;
    private ImageView mSaveIv;
    private TextView mLogoutTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mPhraseEt = (EditText) findViewById(R.id.phrase_et);
        mSaveIv = (ImageView) findViewById(R.id.save_btn);
        mLogoutTv = (TextView) findViewById(R.id.logout_tv);

        String phrase = SharedPreferenceManager.newInstance(this).getSavedValue(Constants.SharedPrefKeys.PHRASE);
        if (phrase.length() != 0) {
            mPhraseEt.setText(phrase);
        }

        mSaveIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPhraseEt.getText().toString().length() == 0 || mPhraseEt.getText().toString().length() < 15) {
                    mPhraseEt.setError("Phrase should have at least 3 to 6 words");
                } else {
                    SharedPreferenceManager.newInstance(SettingsActivity.this).
                            saveValue(Constants.SharedPrefKeys.PHRASE, mPhraseEt.getText().toString().toLowerCase());
                    mPhraseEt.clearFocus();
                    Toast.makeText(SettingsActivity.this, "Saved your preferences", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mLogoutTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.newInstance(SettingsActivity.this).saveValue(Constants.SharedPrefKeys.USER_ID, "");
                startActivity(new Intent(SettingsActivity.this, SignInActivity.class));
                finish();
            }
        });

        startCommonService();

    }

    private void startCommonService() {
        Intent intentService = new Intent(SettingsActivity.this, CommonService.class);
        startService(intentService);
    }
}
