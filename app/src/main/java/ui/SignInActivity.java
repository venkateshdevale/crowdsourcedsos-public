package ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.weloftlabs.crowdsourcedsos.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import managers.SharedPreferenceManager;
import utils.Constants;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */
public class SignInActivity extends BaseActivity {

    private Toolbar mToolbar;
    private Button mLoginbtn;
    private Button mSignupBtn;
    private EditText mFullnameEt;
    private EditText mPasswordEt;
    private EditText mPhoneNumberEt;
    private EditText mAdhaarNumberEt;
    private EditText mGuardianNameEt;
    private EditText mGuardianPhoneEt;
    private TextView mAlertTv;
    private SimpleDraweeView mProfilePicIv;
    private boolean isSignup = false;
    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;
    private ParseFile file;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Check whether the user is Logged In.
         */
        if (SharedPreferenceManager.newInstance(this)
                .getSavedValue(Constants.SharedPrefKeys.USER_ID).trim().length() != 0) {
            startActivity(new Intent(this, SettingsActivity.class));
            finish();
        }

        setContentView(R.layout.activity_signin);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mFullnameEt = (EditText) findViewById(R.id.username);
        mPasswordEt = (EditText) findViewById(R.id.password);
        mPhoneNumberEt = (EditText) findViewById(R.id.phone_number_et);
        mAdhaarNumberEt = (EditText) findViewById(R.id.adhaar_card_number_et);
        mGuardianNameEt = (EditText) findViewById(R.id.guardian_name_et);
        mGuardianPhoneEt = (EditText) findViewById(R.id.guardian_phone_et);
        mLoginbtn = (Button) findViewById(R.id.login);
        mSignupBtn = (Button) findViewById(R.id.signup);
        mAlertTv = (TextView) findViewById(R.id.alert_tv);
        mProfilePicIv = (SimpleDraweeView) findViewById(R.id.profile_pic_iv);

        mAlertTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isSignup) {
                    isSignup = true;
                    mProfilePicIv.setVisibility(View.VISIBLE);
                    mPhoneNumberEt.setVisibility(View.VISIBLE);
                    mAdhaarNumberEt.setVisibility(View.VISIBLE);
                    mGuardianNameEt.setVisibility(View.VISIBLE);
                    mGuardianPhoneEt.setVisibility(View.VISIBLE);
                    mLoginbtn.setVisibility(View.GONE);
                    mSignupBtn.setVisibility(View.VISIBLE);
                    mAlertTv.setText("Already have an account? Login");
                } else {
                    isSignup = false;
                    mProfilePicIv.setVisibility(View.GONE);
                    mPhoneNumberEt.setVisibility(View.GONE);
                    mAdhaarNumberEt.setVisibility(View.GONE);
                    mGuardianNameEt.setVisibility(View.GONE);
                    mGuardianPhoneEt.setVisibility(View.GONE);
                    mLoginbtn.setVisibility(View.VISIBLE);
                    mSignupBtn.setVisibility(View.GONE);
                    mAlertTv.setText("Don't have an account? SignUp");
                }
            }
        });

        mLoginbtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                doLogin();
            }
        });

        mSignupBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                final String mUserNameTv = mFullnameEt.getText().toString();
                String mPasswordTv = mPasswordEt.getText().toString();

                if (file == null) {
                    Toast.makeText(getApplicationContext(), "Please select a profile pic",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                // Force user to fill up the form
                if (mUserNameTv.equals("") && mPasswordTv.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please complete the sign up form",
                            Toast.LENGTH_LONG).show();

                } else {
                    // Save new user data into Parse.com Data Storage
                    final ParseUser user = new ParseUser();
                    user.setUsername(mFullnameEt.getText().toString().trim());
                    user.setPassword(mPasswordEt.getText().toString().trim());
                    user.signUpInBackground(new SignUpCallback() {
                        public void done(com.parse.ParseException e) {
                            if (e == null) {
                                Toast.makeText(getApplicationContext(), "Successfully signed up.",
                                        Toast.LENGTH_LONG).show();
                                SharedPreferenceManager.newInstance(SignInActivity.this)
                                        .saveValue(Constants.SharedPrefKeys.USER_ID, user.getObjectId());
                                saveSignUpData();

                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Sign up error", Toast.LENGTH_LONG)
                                        .show();
                            }
                        }
                    });
                }

            }
        });

        mProfilePicIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);
            }
        });

    }

    private void saveSignUpData() {
        final ParseObject userDetails = new ParseObject("UserDetails");
        userDetails.put("userId", SharedPreferenceManager.newInstance(this).getSavedValue(Constants.SharedPrefKeys.USER_ID));
        userDetails.put("name", mFullnameEt.getText().toString().trim());
        userDetails.put("imageFile", file);
        userDetails.put("phoneNumber", mPhoneNumberEt.getText().toString().trim());
        userDetails.put("adhaarId", mAdhaarNumberEt.getText().toString().trim());
        userDetails.put("guardianName", mGuardianNameEt.getText().toString().trim());
        userDetails.put("guardianPhoneNumber", mGuardianPhoneEt.getText().toString().trim());

        userDetails.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                doLogin();

            }
        });
    }

    private void doLogin() {
        // Send data to Parse.com for verification
        ParseUser.logInInBackground(mFullnameEt.getText().toString().trim(), mPasswordEt.getText().toString().trim(),
                new LogInCallback() {
                    public void done(ParseUser user, com.parse.ParseException e) {
                        if (user != null) {
                            // If user exist and authenticated, send user to Settings.class
                            SharedPreferenceManager.newInstance(SignInActivity.this).saveValue(Constants.SharedPrefKeys.USER_ID
                                    , user.getObjectId());

                            Intent intent = new Intent(SignInActivity.this, SettingsActivity.class);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Successfully logged in", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "No such user exist, please signup",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();

                Uri uri = Uri.parse(selectedImageUri.toString());
                mProfilePicIv.setImageURI(uri);

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    saveProfilePicToParse(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }


    private void saveProfilePicToParse(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] image = stream.toByteArray();

        file = new ParseFile("profilePic.png", image);
        file.saveInBackground();

    }
}
