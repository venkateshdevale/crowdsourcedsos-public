package services;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class CommonService extends Service {

    public static Context mContext;
    private BroadcastReceiver mReceiver;
    private final String ACTION_SCREEN_OFF = "android.intent.action.SCREEN_OFF";
    private final String ACTION_SCREEN_ON = "android.intent.action.SCREEN_ON";

    public CommonService() {
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();

        mContext = this;
        registerScreenEventReceiver();
    }

    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
        mContext = null;
    }

    /**
     * Registers a receiver which receives the screen event broadcast.
     */
    private void registerScreenEventReceiver() {
        IntentFilter intentfilter = new IntentFilter(ACTION_SCREEN_ON);
        intentfilter.addAction(ACTION_SCREEN_OFF);
        mReceiver = new ScreenEventsReceiver();
        registerReceiver(mReceiver, intentfilter);
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    public static Context getContext() {
        return mContext;
    }

}