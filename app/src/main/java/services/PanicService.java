package services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by sree on 3/10/15.
 */
public class PanicService extends Service {
    private static final String TAG = "PANIC_SERVICE";

    public static String KILL_PANIC_SERVICES = "kill_panic_services";

    @Override
    public void onCreate() {
        super.onCreate();

        LocalBroadcastManager.getInstance(this).registerReceiver(mkillServiceReceiver, new IntentFilter(KILL_PANIC_SERVICES));
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public BroadcastReceiver mkillServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "Killing service ...");
            stopSelf();

        }


    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mkillServiceReceiver);
    }
}
