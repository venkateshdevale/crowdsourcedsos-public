package services;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ui.LockScreenActivity;

public class ScreenEventsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            if (LockScreenActivity.getContext() == null) {
                Intent i = new Intent(context, LockScreenActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
        }

    }

}