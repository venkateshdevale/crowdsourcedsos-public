package services;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.weloftlabs.crowdsourcedsos.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import managers.SharedPreferenceManager;
import ui.MapsActivity;
import utils.Constants;
import utils.NotificationUtils;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */

public class CustomPushReceiver extends ParsePushBroadcastReceiver {
    private final String TAG = CustomPushReceiver.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private Intent parseIntent;

    public CustomPushReceiver() {
        super();
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        if (intent == null)
            return;

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            Log.e(TAG, "Push received: " + json);
            parseIntent = intent;
            parsePushJson(context, json);
        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    /**
     * Parses the push notification json
     *
     * @param context
     * @param json
     */
    private void parsePushJson(final Context context, final JSONObject json) {
        try {
            final String userId = json.getString(Constants.SharedPrefKeys.USER_ID);

            if (userId.equalsIgnoreCase(SharedPreferenceManager.newInstance(context)
                    .getSavedValue(Constants.SharedPrefKeys.USER_ID)))
                return;

            ParseQuery<ParseUser> query = ParseQuery.getQuery("_User");
            query.getInBackground(userId, new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    showNotificationForJson(context, userId, parseUser.getString("userName"));
                }
            });

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }


    /**
     * Shows the notification message in the notification bar
     * If the app is in background, launches the app
     *
     * @param context
     * @param title
     * @param message
     * @param intent
     */
    private void showNotificationMessage(Context context, String title, String message, Intent intent) {

        notificationUtils = new NotificationUtils(context);
        intent.putExtras(parseIntent.getExtras());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent);
    }

    private void showNotificationForJson(Context context, String userId, String userName) {

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(Constants.SharedPrefKeys.USER_ID, userId);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification;

        if (Build.VERSION.SDK_INT < 16) {
            notification = new Notification.Builder(context).setContentTitle(userName + " is calling for help.")
                    .setContentText("Tap to track and help the victim")
                    .setSmallIcon(R.drawable.ic_launcher)
                    .getNotification();
        } else {
            notification = new Notification.Builder(context)
                    .setContentTitle(userName + " is calling for help.")
                    .setContentText("Tap to track and help the victim")
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setSound(soundUri)
                    .addAction(0, "Provide help", pendingIntent)
                    .build();
        }

        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);


    }
}