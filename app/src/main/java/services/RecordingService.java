package services;

import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import managers.SharedPreferenceManager;
import utils.Constants;
import utils.Logger;

/**
 * Created by sree on 3/10/15.
 */
public class RecordingService extends PanicService {

    private File mfile;
    private MediaRecorder mRecorder = null;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Logger.d("Audio recording Stopped");
            stopRecording();
            pushToCloud();
            startRecordingCrimeSceneAudio();
        }


    };

    private void pushToCloud() {

        Logger.d("Pushing to cloud");


        try {
            byte[] bytes = FileUtils.readFileToByteArray(mMediaFile);

            Logger.d("Creating Parse object");

            final ParseObject record = new ParseObject("VictimAudioRecord");

            ParseFile file = new ParseFile("victim_record.3gp", bytes);
            record.put("userId", SharedPreferenceManager.newInstance(this).getSavedValue(Constants.SharedPrefKeys.USER_ID));
            record.put("victim_record", file);

            record.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {

                    Logger.d("Audio recording saved to Cloud ");

                }
            });

        } catch (IOException e) {
            e.printStackTrace();
            Logger.d("Audio recording saved to Cloud  Failed " + e.getMessage());


        }


    }

    private File mMediaFile;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isExternalStorageWritable()) {
            startRecordingCrimeSceneAudio();
        }
        return START_STICKY;
    }


    private void startRecordingCrimeSceneAudio() {

        mfile = new File(Environment.getExternalStorageDirectory(), "/panic/");
        mfile.mkdirs();
        mMediaFile = new File(mfile.getAbsolutePath()//folder path
                + File.separator
                + System.currentTimeMillis() + ".3gp");
        try {
            //mMediaFile name

            mMediaFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }


        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mMediaFile.getPath());
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        Logger.d("Audio recording to mMediaFile : " + mMediaFile.getPath());

        try {

            mRecorder.prepare();
        } catch (IOException e) {
            Logger.d("prepare() failed " + e.getMessage());
        }

        mRecorder.start();

        mHandler.sendEmptyMessageDelayed(0, 1000 * 60);

        Logger.d("Audio recording started");

    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRecording();
    }

}
