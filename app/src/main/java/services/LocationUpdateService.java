package services;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import managers.SharedPreferenceManager;
import utils.Constants;
import utils.Logger;

/**
 * Created by sree on 3/10/15.
 */
public class LocationUpdateService extends PanicService implements LocationListener {
    private static final String TAG = "LOCATION";
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Location service called");

        Constants.sLocationObjectId = "";
        buildGoogleApiClient();
        return START_STICKY;
    }


    private void fetchLastKnownLocation() {

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d(TAG, "Loc Lat" + String.valueOf(mLastLocation.getLatitude()));
            Log.d(TAG, "Loc Lat" + String.valueOf(mLastLocation.getLongitude()));
        }


    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Log.d(TAG, "API client connected");
                        fetchLastKnownLocation();
                        createLocationRequest();
                        startLocationUpdates();

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {

                    }
                })
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setSmallestDisplacement(2);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
        mGoogleApiClient = null;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location updates  " + location.getLatitude());

        if (Constants.sLocationObjectId == null || Constants.sLocationObjectId.length() == 0) {
            startLocationRecordingOnParse(location);
        } else {
            updateLocationOnParse(location);
        }
    }

    private void startLocationRecordingOnParse(Location location) {
        final ParseObject record = new ParseObject("VictimLocationRecord");

        ParseGeoPoint point = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
        record.put("userId", SharedPreferenceManager.newInstance(this).getSavedValue(Constants.SharedPrefKeys.USER_ID));
        record.put("victimLocationRecord", point);

        record.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Constants.sLocationObjectId = record.getObjectId();
                Logger.d("Location pushed to cloud  -->record.getObjectId()" + record.getObjectId());

                String err = "";
                if (e != null) {
                    err = e.toString();
                }

                Toast.makeText(LocationUpdateService.this, "Start Loc" + err, Toast.LENGTH_SHORT).show();

                // Sending the push to other to get help.
                ParsePush push = new ParsePush();
                JSONObject data = new JSONObject();

                try {
                    data.put(Constants.SharedPrefKeys.USER_ID,
                            SharedPreferenceManager.newInstance(LocationUpdateService.this).
                                    getSavedValue(Constants.SharedPrefKeys.USER_ID));

                } catch (JSONException exception) {
                    exception.printStackTrace();
                }

                push.setData(data);
                push.sendInBackground(new SendCallback() {
                    @Override
                    public void done(ParseException e) {

                        if (e != null)
                            Logger.d(e.toString());
                    }
                });
            }
        });


    }

    private void updateLocationOnParse(final Location location) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("VictimLocationRecord");

        // Retrieve the object by id
        query.getInBackground(Constants.sLocationObjectId, new GetCallback<ParseObject>() {
            public void done(final ParseObject record, ParseException e) {
                if (e == null) {

                    ParseGeoPoint point = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
                    record.put("userId",
                            SharedPreferenceManager.newInstance(LocationUpdateService.this).
                                    getSavedValue(Constants.SharedPrefKeys.USER_ID));
                    record.put("victimLocationRecord", point);
                    record.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            String err = "";
                            if (e != null)
                                err = e.toString();
                            Toast.makeText(LocationUpdateService.this, "Updated Loc" + err, Toast.LENGTH_SHORT).show();
                            Logger.d("Location updated to cloud  -->record.getObjectId()" + record.getObjectId());
                        }
                    });
                }
            }
        });
    }


}
