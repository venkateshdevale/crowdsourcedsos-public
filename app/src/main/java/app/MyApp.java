package app;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import utils.Constants;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */
public class MyApp extends Application {

    private static String TAG = "WLL";
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;

        Fresco.initialize(sContext);

        Parse.initialize(this, Constants.ParseKeys.APPLICATION_ID, Constants.ParseKeys.CLIENT_KEY);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

    }

    public static String getTag() {
        return TAG;
    }

    public static Context getContext() {
        return sContext;
    }

}
