package managers;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */
public abstract class BaseManager {
    public abstract void cleanUp();
}
