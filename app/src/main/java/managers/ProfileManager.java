package managers;

import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import model.User;

/**
 * Created by sree on 4/10/15.
 */
public class ProfileManager {

    public interface ProfileDownloaderCallback {

        void onDownloadProfile(User user);

        void onDownloadLocationList(List<ParseObject> locationsList);

        void onDownloadAudioList(List<ParseObject> audioList);
    }


    /**
     * Get the profile details of the victim
     *
     * @param userId
     * @return
     */
    public static void getVictimProfileDetails(String userId, final ProfileDownloaderCallback profileDownloaderCallback) {
        final User user = new User();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserDetails");
        query.whereEqualTo("userId", userId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> userDetailsList, ParseException e) {
                if (e == null) {
                    if (userDetailsList.size() != 0) {
                        ParseObject userDetailsObj = userDetailsList.get(0);

                        user.name = userDetailsObj.getString("name");
                        user.contactNo = userDetailsObj.getString("phoneNumber");
                        user.ecContact = userDetailsObj.getString("guardianPhoneNumber");
                        ParseFile imageFile = (ParseFile) userDetailsObj.get("imageFile");
                        if (imageFile != null)
                            user.url = imageFile.getUrl();
                        if (profileDownloaderCallback != null)
                            profileDownloaderCallback.onDownloadProfile(user);

                    }
                } else {
                    // Something went wrong
                }
            }
        });

    }


    /**
     * Get the location details list of the victim
     *
     * @param userId
     * @return
     */
    public static void getVictimLocationList(String userId, final ProfileDownloaderCallback profileDownloaderCallback) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("VictimLocationRecord");

        query.whereEqualTo("userId", userId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> locationsList, ParseException e) {
                if (e == null) {
                    if (locationsList.size() != 0) {

                        if (profileDownloaderCallback != null)
                            profileDownloaderCallback.onDownloadLocationList(locationsList);

                    }
                } else {
                    // Something went wrong
                }
            }
        });

    }

    /**
     * Get the recorded audio list of the victim
     *
     * @param userId
     * @return
     */
    public static void getVictimAudioList(String userId, final ProfileDownloaderCallback profileDownloaderCallback) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("VictimAudioRecord");

        query.whereEqualTo("userId", userId);
        query.setLimit(10);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> audioList, ParseException e) {
                if (e == null) {
                    if (audioList != null) {

                        if (profileDownloaderCallback != null)
                            profileDownloaderCallback.onDownloadAudioList(audioList);

                    }
                } else {
                    // Something went wrong
                }
            }
        });

    }


}
