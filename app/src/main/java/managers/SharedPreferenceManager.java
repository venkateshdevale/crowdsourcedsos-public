package managers;

import android.content.Context;
import android.content.SharedPreferences;

import utils.Constants;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */
public class SharedPreferenceManager extends BaseManager {
    private static SharedPreferenceManager sSharedPreferenceManager;
    private SharedPreferences mSharedPreference;

    private SharedPreferenceManager(Context context) {
        mSharedPreference = context.getSharedPreferences(Constants.SharedPrefKeys.SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE);
    }

    public static SharedPreferenceManager newInstance(Context context) {
        if (sSharedPreferenceManager == null) {
            sSharedPreferenceManager = new SharedPreferenceManager(context);
        }

        return sSharedPreferenceManager;
    }

    public void saveValue(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getSavedValue(String key) {
        return mSharedPreference.getString(key, "");
    }

    @Override
    public void cleanUp() {
        sSharedPreferenceManager = null;
    }
}