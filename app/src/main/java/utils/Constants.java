package utils;

/**
 * Created by Venkatesh Devale on 03-10-2015.
 */
public class Constants {
    public static String sLocationObjectId = "";

    public interface ParseKeys {
        String APPLICATION_ID = "blcxsrTrifogJeK5wj5nZIAdnkj5ne4SrhkiAmZV";
        String CLIENT_KEY = "xf3Sfi6epUaKJswSYxmxpvOdau9ud6m4HxHu20GX";
    }

    public interface SharedPrefKeys {
        String SHARED_PREFERENCE_KEY = "MY_SHARED_PREF";
        String PHRASE = "phrase";
        String USER_ID = "objectId"; // Its the userId
        String LOCATION_OBJECT_ID = "locationObjectId";
    }
}
